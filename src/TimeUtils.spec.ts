import { TimeUtils } from './TimeUtils';

import * as test from 'tape';

test('TimeUtils Unit Tests', t => {
  t.is(
    TimeUtils.getTimezone(),
    TimeUtils.DEFAULT_TIME_ZONE,
    'returns the default timezone if not set'
  );
  TimeUtils.setTimezone('America/Toronto');
  t.is(
    TimeUtils.getTimezone(),
    'America/Toronto',
    'changes the timezone when set'
  );
  TimeUtils.setTimezone(TimeUtils.DEFAULT_TIME_ZONE);

  t.is(TimeUtils.timestamp().length > 0, true, 'returns a non empty timestamp');

  let end = TimeUtils.daysFromToday(3);
  t.is(end.isAfter(TimeUtils.today()), true, 'returns a date in the future');
  t.is(
    TimeUtils.getDays(TimeUtils.today(), end).length,
    4,
    'returns the correct dates into the future'
  );
  end = TimeUtils.daysBeforeToday(4);
  t.is(end.isBefore(TimeUtils.today()), true, 'returns a date in the past');
  t.is(
    TimeUtils.getDays(TimeUtils.today(), end).length,
    5,
    'returns the correct dates into the past'
  );

  let today = TimeUtils.roundToTheDay(TimeUtils.now());
  t.is(
    today.hour() == 0 &&
      today.minutes() == 0 &&
      today.seconds() == 0 &&
      today.milliseconds() == 0,
    true,
    'rounds off all fields to the day'
  );

  t.end();
});
