import { Moment } from 'moment-timezone';
import * as moment from 'moment-timezone';

import { CommonsUtils } from '@codificationorg/commons';

export class TimeUtils {
  public static ENV_TIMEZONE = 'TIMEZONE';

  public static DEFAULT_TIME_ZONE = 'America/Los_Angeles';

  public static FORMAT_TIMESTAMP = 'YYYY-MM-DD HH:mm:ss,SSS';

  public static UNIT_DAY = 'days';

  private static TIMEZONE = CommonsUtils.env(
    TimeUtils.ENV_TIMEZONE,
    TimeUtils.DEFAULT_TIME_ZONE
  );

  public static setTimezone(timezone: string): void {
    TimeUtils.TIMEZONE = timezone;
  }

  public static getTimezone(): string {
    return TimeUtils.TIMEZONE;
  }

  public static timestamp(): string {
    return this.now().format(TimeUtils.FORMAT_TIMESTAMP);
  }

  public static getDays(start: Moment, end: Moment): Moment[] {
    const rval: Moment[] = [];
    const first = this.roundToTheDay(start);
    const last = this.roundToTheDay(end);
    rval.push(first);
    let current = first;
    while (current.unix() !== last.unix()) {
      if (first.isBefore(last)) {
        current = current.add(1 as any, this.UNIT_DAY);
      } else {
        current = current.subtract(1 as any, this.UNIT_DAY);
      }
      current = this.roundToTheDay(current);
      rval.push(current);
    }
    return rval;
  }

  public static daysBeforeToday(days: number): Moment {
    return this.today().subtract(days as any, this.UNIT_DAY);
  }

  public static daysFromToday(days: number): Moment {
    return this.today().add(days as any, this.UNIT_DAY);
  }

  public static today(): Moment {
    return this.roundToTheDay(this.now());
  }

  public static roundToTheDay(date: Moment): Moment {
    return date
      .clone()
      .hour(0)
      .minute(0)
      .second(0)
      .millisecond(0);
  }

  public static now(): Moment {
    return moment.tz(this.getTimezone());
  }
}
